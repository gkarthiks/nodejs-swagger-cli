#!/bin/bash
set -e

if [[ $# -lt 1 ]];
    then echo "No directories to scan and validate."
	exit 1
fi

for var in "$@"
do
    echo "$var"
	for file in "$var"/*
	do
		if [[ $file == *.yaml ]] || [[ $file == *.yml ]]; then
			echo "Validating the $file"
			swagger-cli validate $file
		fi
	done    
done
